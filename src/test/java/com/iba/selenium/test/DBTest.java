package com.iba.selenium.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class DBTest {

	private static Connection connection;

	@BeforeClass
	public static void init() {
		try {
			ResourceBundle resource = ResourceBundle.getBundle("database");
			String jdbcUrl = getUrl(resource);
			String user = resource.getString("mySQLUser");
			String pwd = resource.getString("mySQLPassword");
			connection = DriverManager.getConnection(jdbcUrl, user, pwd);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void connectionNotNull() {
		Assert.assertNotNull(connection);
	}

	@AfterClass
	public static void closeConnection() throws SQLException {
		connection.close();
	}

	private static String getUrl(ResourceBundle resource) {
		return "jdbc:" + resource.getString("mySQLDBMS") + "://" + resource.getString("mySQLHost") + ":"
				+ resource.getString("mySQLPort") + "/" + resource.getString("mySQLDB") + "?"
				+ resource.getString("mySQLParams");
	}

}
