package com.iba.selenium.test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeleniumRunnerTest {

	static WebDriver driver;
	
	@BeforeClass
	public static void init() throws MalformedURLException {
//		ChromeOptions options = new ChromeOptions();
//		options.addArguments("disable-infobars");
		DesiredCapabilities capability = DesiredCapabilities.chrome();

        driver = new RemoteWebDriver(new java.net.URL("http://172.16.4.55:4444/wd/hub"), capability);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
	}
	
	@Test
	public void openStartPage() {
		driver.get("https://www.google.com/");
	}
	
	@AfterClass
	public static void shutDownDriver() {
	    driver.quit();
	}
}
